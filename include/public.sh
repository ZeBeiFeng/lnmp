#!/bin/bash
#####################################################################
# LNMP is a bash script for the installation of Nginx + PHP + MySQL.#
# Author        Hehl <1181554113@qq.com>                            #
# Blog          Http://www.yunweijilu.com                           #
# Desribtion:		Public Function				    #
#####################################################################

#Sync time
sync_time(){
case $sync_time_yn in
y|Y)
	. ./include/sync_time.sh
;;
*)
	echo "Not install or input wrong value for sync time!"
;;
esac
}

#install openssl module
install_openssl(){
pushd $src_dir
tar xzf ${openssl}.tar.gz
cd ${openssl}
./configure
make && make install
popd
}

#Install zlib for ubuntu
install_zlib(){
pushd $src_dir
tar xJf ${zlib}.tar.xz && cd $zlib
./configure
make && make install
popd
}

#install jemalloc for nginx and mysql
install_jemalloc(){
pushd $src_dir
tar xjf ${jemalloc}.tar.bz2 && cd $jemalloc
./configure --prefix=/usr/local/jemalloc --libdir=/usr/local/lib
make && make install
make clean
popd
echo "/usr/local/lib" > /etc/ld.so.conf.d/usr_local_lib.conf
/sbin/ldconfig
}

#Download
down_url(){
wget -c --no-check-certificate $*
}

#Unzip Notice
unzip_notice(){
echo -e "\033[35mNow it's decompress file,please wait...\033[0m"
}

#Mysql Insltall Way
mysql_install_way(){
echo
echo  -e "${YELLOW}How to install Mysql Server? :
		$RED 1)$WHITE Use Binary File;(Default)
		$RED 2)$WHITE Use Build Source Code;(Need more time)"
echo
read -p "You select the number to install mysql:  " mysql_install_wayway
}

#Before install mysql
before_install_mysql(){
cp -f conf/my.cnf /etc
[ "$mysql_chk_u" -eq 0 ] && useradd -M -s /sbin/nolgin mysql
[ -f /etc/my.cnf ] && mv -f /etc/my.cnf /etc/my.cnf_`date +%Y%m%d%H%M`
if [ -d $mysql_local ];then
	/etc/init.d/mysqld start >/dev/null 2>&1
	$MYSQLDUMP -u$MYSQLUSR -p${mysql_root_pass} --all-databases > $mysql_data_backup
	/etc/init.d/mysqld stop && echo "Now is stoping mysql..."
	mv -f $mysql_local $mysql_old_dir
fi
[ -d $mysql_data ] && mv -f $mysql_data $data_backup_dir/mysql_`date +%Y%m%d%H%M`
}

#Install MYSQL
install_mysql56_cmake(){
if [ $os == "centos" ];then
	yum -y install make gcc-c++ cmake bison-devel  ncurses-devel
else
	apt-get update -y
	apt-get install -y -f build-essential mlocate net-tools bzip2 autoconf make cmake  gcc screen psmisc
	apt-get install -y libncurses5-dev
fi
pushd $src_dir 
if [ ! -f ${mysql_gz[1]} ];then
	down_url ${mysql_down_url[1]}/${mysql_gz[1]}
	if [ $? -ne 0 ];then
		echo "Download ${mysql_gz[1]} failed,please contact author!"
		exit
	fi
fi
unzip_notice
tar  xzf ${mysql_gz[1]}
cd ${mysql_version[1]}
#make clean && rm -rf CMakeCache.txt
cmake . -DCMAKE_INSTALL_PREFIX=$mysql_local \
-DMYSQL_DATADIR=$mysql_data \
-DSYSCONFDIR=/etc \
-DWITH_MYISAM_STORAGE_ENGINE=1 \
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
-DWITH_MEMORY_STORAGE_ENGINE=1 \
-DWITH_READLINE=1 \
-DMYSQL_UNIX_ADDR=/var/lib/mysql/mysql.sock \
-DMYSQL_TCP_PORT=3306 \
-DENABLED_LOCAL_INFILE=1 \
-DWITH_PARTITION_STORAGE_ENGINE=1 \
-DEXTRA_CHARSETS=all \
-DDEFAULT_CHARSET=utf8mb4 \
-DDEFAULT_COLLATION=utf8mb4_general_ci
make -j `grep processor /proc/cpuinfo | wc -l` && make install
rm -rf ${mysql_version[1]} ${mysql_gz[1]}
popd
}

#Modify password then Grant privileges
mysql_grant(){
$mysql_admin -uroot password "${mysql_root_pass}"
if [ $mysql_version_select -eq 1 ];then
	cp -f include/grant56.sql include/grant56.bak
	sed -i 's@mysql_pwd@'"${mysql_root_pass}"'@g' include/grant56.sql
	cat include/grant56.sql | $mysql_cmd -uroot -p${mysql_root_pass}
	mv -f include/grant56.bak include/grant56.sql
elif [ $mysql_version_select -eq 2 ];then
	cp -f include/grant57.sql include/grant57.bak
	sed -i 's@mysql_pwd@'"${mysql_root_pass}"'@g' include/grant57.sql
	cat include/grant57.sql | $mysql_cmd -uroot -p${mysql_root_pass}
	mv -f include/grant57.bak include/grant57.sql
fi
[ -f $mysql_data_backup ] && $mysql_cmd -u$MYSQLUSR -p$mysql_root_pass < $mysql_data_backup
}

#Mysql install use binary need errmsg.sys
cp_errmsg(){
[ ! -d /usr/share/mysql ] && mkdir -p /usr/share/mysql
if [ ! -f /usr/share/mysql/errmsg.sys ];then
	cp -f $mysql_local/share/english/errmsg.sys /usr/share/mysql >/dev/null 2>&1
	cp /usr/share/mysql/english/errmsg.sys /usr/share/mysql >/dev/null 2>&1
	cp -f $mysql_local/share/errmsg.sys /usr/share/mysql >/dev/null 2>&1
fi
[ -f /etc/mysql/my.cnf ] && mv /etc/mysql/my.cnf /etc/mysql/my.cnf_$RANDOM
}

mysql_install_boot(){
cd $mysql_local
chown -R mysql:mysql $mysql_local && chmod 755 $mysql_local/bin/*
cp -rf ${mysql_local}/support-files/mysql.server /etc/init.d/mysqld && chmod 755 /etc/init.d/mysqld
sed -i "s:^basedir=.*:basedir=$mysql_local:g" /etc/init.d/mysqld
sed -i "s:^datadir=.*:datadir=$mysql_data:g" /etc/init.d/mysqld
echo -e "$WHITE"
[ ! -d $mysql_data ] && mkdir -p $mysql_data && chown -R mysql.mysql $mysql_data
[ -d /var/lib/mysql ] && rm -rf /var/lib/mysql
cd -
if [ $mysql_version_select -eq 1 ];then
	cp -rf conf/my.cnf /etc
	$mysql_local/scripts/mysql_install_db --basedir=$mysql_local --datadir=$mysql_data --user=mysql
elif [ $mysql_version_select -eq 2 ];then
	cp -rf conf/my57.cnf /etc/my.cnf
	/usr/local/mysql/bin/mysqld --initialize-insecure --user=mysql --basedir=$mysql_local --datadir=$mysql_data
fi
if [ $os == "centos" ];then
	chkconfig mysqld on && chkconfig save
elif [ $os == "ubuntu" ];then
	update-rc.d mysqld defaults
fi

#Add mysql to enviroment path
if [[ `sed -n '/export PATH=*.*\/mysql\/bin/p' /etc/profile | wc -l` -gt 1 ]];then
	sed -i '/export PATH=*.*\/mysql\/bin/d' /etc/profile
	echo "export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin:/usr/local/mysql/bin" >> /etc/profile
else
	echo "export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin:/usr/local/mysql/bin" >> /etc/profile
fi

/etc/init.d/mysqld start
mysql_grant
if [ $os == "ubuntu" ];then
	echo "/usr/local/mysql" > /etc/ld.so.conf.d/mysql.conf
	ldconfig
fi
}

#install mysql 5.7 by cmake
install_mysql57_cmake(){
pushd $src_dir
useradd -M -s /sbin/nologin mysql
if [ $os == "centos" ];then
	yum install -y gcc gcc-c++ ncurses ncurses-devel cmake libaio
else
	apt-get update -y
	apt-get install -y -f build-essential mlocate net-tools bzip2 autoconf make cmake  gcc screen psmisc
	apt-get install -y libncurses5-dev
fi
if [ ! -f ${mysql_gz[2]} ];then
	down_url ${mysql_down_url[2]}/${mysql_gz[2]}
	if [ $? -ne 0 ];then
		echo "Donwload ${mysql_gz[2]} failed,please contact author!"
		exit
	fi
fi
if [ ! -f ${mysql_gz[5]} ];then
	down_url ${mysql_down_url[2]}/${mysql_gz[5]}
	if [ $? -ne 0 ];then
		echo "Donwload ${mysql_gz[5]} failed,please contact author!"
		exit
	fi
fi
unzip_notice
tar xzf ${mysql_gz[2]}
tar xzf ${mysql_gz[5]}
cd ${mysql_dir[2]}
cmake . -DCMAKE_INSTALL_PREFIX=/usr/local/mysql \
-DMYSQL_DATADIR=/data/mysql \
-DDOWNLOAD_BOOST=1 \
-DWITH_BOOST=boost/boost_1_59_0 \
-DSYSCONFDIR=/etc \
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
-DWITH_PARTITION_STORAGE_ENGINE=1 \
-DWITH_FEDERATED_STORAGE_ENGINE=1 \
-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
-DWITH_MYISAM_STORAGE_ENGINE=1 \
-DENABLED_LOCAL_INFILE=1 \
-DENABLE_DTRACE=0 \
-DDEFAULT_CHARSET=utf8mb4 \
-DDEFAULT_COLLATION=utf8mb4_general_ci \
-DWITH_EMBEDDED_SERVER=1
make -j `grep processor /proc/cpuinfo | wc -l`
make install
echo -e "$WHITE"
rm -rf ${mysql_version[2]} ${mysql_gz[2]}
popd
cp -rf $mysql_local/support-files/mysql.server /etc/init.d/mysqld
}

#Install mysql-5.6 by binary file
install_mysql56_bin(){
pushd $src_dir
yum install wget autoconf -y >/dev/null 2>&1
apt-get install wget -y >/dev/null 2>&1
yum install -y libaio  numactl.x86_64
if [ $sys_bit == "i386" ];then
	yum install numactl
fi
if [ ! -f ${mysql_gz[3]} ];then
	down_url ${mysql_down_url[1]}/${mysql_gz[3]}
	if [ $? -ne 0 ];then
		echo "Download ${mysql_gz[3]} Failed,Please contact Author."
		exit
	fi
fi
[ ! -d ${mysql_dir[3]} ] && tar xzf ${mysql_gz[3]}
mv -f ${mysql_dir[3]} $mysql_local
cp_errmsg
popd
}

#Install mysql-5.7 by binary file
install_mysql57_bin(){
pushd $src_dir
yum install -y libaio autoconf
if [ ! -f ${mysql_gz[4]} ];then
	down_url ${mysql_down_url[2]}/${mysql_gz[4]}
	if [ $? -ne "0" ];then
        	echo "Download ${mysql_gz[3]} Failed,Please contact Author."
        	exit
		fi
fi
if [ ! -d ${mysql_dir[4]} ];then
	unzip_notice
	tar xzf ${mysql_gz[4]}
fi
mv -f ${mysql_dir[4]} $mysql_local
cp_errmsg
popd
}

#Install mysql5.6 in Ubuntu by binary file
install_mysql56_bin_u(){
apt-cache search libaio && apt-get install -y libaio1 libaio-dev numactl
if [ "$mysql_chk_u" -eq 0 ];then
	groupadd mysql
	useradd -r -g mysql -s /bin/false mysql
fi
pushd $src_dir
if [ ! -f ${mysql_gz[3]} ];then
	down_url ${mysql_down_url[1]}/${mysql_gz[3]}
	if [ $? -ne 0 ];then
		echo "Download ${mysql_gz[3]} failed,please contact author!"
		exit
	fi
fi
if [ ! -d ${mysql_dir[3]} ];then
	unzip_notice
	tar xzf ${mysql_gz[3]}
fi
mv -f ${mysql_dir[3]} $mysql_local
cp_errmsg
popd
}

#Install mysql5.7 in Ubuntu by binary file
install_mysql57_bin_u(){
pushd $src_dir
apt-cache search libaio && apt-get install -y libaio1 libaio-dev
if [ "$mysql_chk_u" -eq 0 ];then
	groupadd mysql
	useradd -r -g mysql -s /bin/false mysql
fi
apt-get update
if [ ! -f ${mysql_gz[4]} ];then
	down_url ${mysql_down_url[2]}/${mysql_gz[4]}
	if [ $? -ne 0 ];then
		echo "Download ${mysql_gz[4]} failed,please contact author!"
	fi
fi
	
if [ ! -d ${mysql_dir[4]} ];then
	unzip_notice
	tar xzf ${mysql_gz[4]}
fi
echo -e "Now copy mysql to $mysql_local"
mv -f ${mysql_dir[4]} $mysql_local
mkdir ${mysql_local}/mysql-files
chmod 750 ${mysql_local}/mysql-files
popd
}

#Install curl
install_curl(){
pushd $src_dir
tar xjf curl-7.56.0.tar.bz2
cd curl-7.56.0
./configure --prefix=/usr/local/curl
make && make install
rm -rf curl-7.56.0
popd
}

#Prepare install php
preinstall_php(){
[ $php_user -eq 0 ] && useradd -M -s /sbin/nologin www
install_curl
if [ $os == "centos" ];then
yum install -y gcc gcc-c++ libxml2 libxml2-devel libjpeg-devel libpng-devel freetype-devel openssl-devel libcurl-devel libmcrypt libmcrypt-devel libicu-devel libxslt-devel openldap openldap-devel
elif [ $os == "ubuntu" ];then
apt-get update
apt-get install  libxml2  libxml2-dev -y
apt-get install  openssl libssl-dev -y
apt-get install  curl libcurl4-gnutls-dev libldap-2.4-2 libldap2-dev  -y
apt-get install libjpeg-dev libpng12-dev   libxpm-dev libfreetype6-dev  libmcrypt-dev  libmysql++-dev  libxslt1-dev  libicu-dev -y 
ln -sf /usr/lib/${sys_bit}-linux-gnu/libssl.so  /usr/lib
fi
}

#Install libmcrypt
install_libmcrypt(){
pushd $src_dir
tar xzf ${libmcrypt}.tar.gz && cd $libmcrypt
./configure
make && make install
popd
}

#check php install status
chk_php_status(){
if [ -e ${php_install_dir_use}/bin/phpize ];then
	echo -e "Php install successful!"
else
	echo -e "${RED}Php install failed, Please contact author.${WHITE}"
	exit || kill -9 $$
fi
}

#Configure for php
config_php(){
#fix curl missing for debian
if [ `grep -i debian /etc/issue | wc -l` -eq 1 ];then
	ln -fs /usr/include/x86_64-linux-gnu/curl /usr/local/include/curl
	apt-get install -y libjpeg-dev libpng-dev  libfreetype6-dev make gcc
elif [ $os == "ubuntu" ];then
	ln -fs /usr/include/${sys_bit}-linux-gnu/curl /usr/include/
	ln -fs /usr/lib/${sys_bit}-linux-gnu/libldap.so /usr/lib/
fi

./configure --prefix=${php_install_dir_use} --with-config-file-path=${php_install_dir_use}/etc \
--with-config-file-scan-dir=${php_install_dir_use}/etc/php.d \
--with-fpm-user=php --with-fpm-group=www --enable-fpm --enable-opcache --disable-fileinfo \
--with-mysql=mysqlnd --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd \
--with-iconv-dir=/usr/local --with-freetype-dir --with-jpeg-dir --with-png-dir --with-zlib \
--with-libxml-dir=/usr --enable-xml --disable-rpath --enable-bcmath --enable-shmop --enable-exif \
--enable-sysvsem --enable-inline-optimization --with-curl=/usr/local/curl --enable-mbregex \
--enable-mbstring --with-mcrypt --with-gd --enable-gd-native-ttf --with-openssl \
--with-mhash --enable-pcntl --enable-sockets --with-xmlrpc --enable-ftp --enable-intl --with-xsl \
--with-gettext --enable-zip --enable-soap --disable-debug
make ZEND_EXTRA_LIBS='-liconv'
make
ln -fs /usr/local/lib/libiconv.so.2 /usr/lib64/
ln -fs /usr/local/lib/libiconv.so.2 /usr/lib/
make install
chk_php_status
cp -f php.ini-production ${php_install_dir_use}/etc/php.ini && cp -f sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm && chmod +x /etc/init.d/php-fpm
}

#Add php boot
add_php_boot(){
if [[ $os == "centos" ]];then
	chkconfig  php-fpm on  && chkconfig save
elif	[[ $os == "ubuntu" ]];then
	update-rc.d php-fpm defaults
fi
ln -fs ${php_install_dir_use}/bin/php /usr/bin/php
}

#Copy php-fpm.conf
copy_php_fpm(){
cp -f conf/php-fpm.conf ${php_install_dir_use}/etc
[ ! -d ${wwwroot_dir} ] && mkdir -p ${wwwroot_dir}/default
service php-fpm restart
rm -rf $src_dir/$libmcrypt
if [ -z $php_version_select ] || [ $php_version_select -eq 1 ];then
	rm -rf $src_dir/${php_version[1]} $src_dir/${php_bz[1]}
else
	rm -rf $src_dir/${php_version[2]} $src_dir/${php_bz[2]}
fi
}
