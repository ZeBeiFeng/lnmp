#!/bin/bash
#####################################################################
# LNMP is a bash script for the installation of Nginx + PHP + MySQL.#
# Author        Hehl <1181554113@qq.com>                            #
# Blog          Http://www.yunweijilu.com                           #
#####################################################################"

clear

before_install_mysql

#Define install mysql5.6
install_mysql56(){
if [[ -z $mysql_install_wayway || $mysql_install_wayway -eq 1 ]];then
	if [ $os == "centos" ];then
		install_mysql56_bin
	elif [ $os == "ubuntu" ];then
		install_mysql56_bin_u
	fi
elif [[ $mysql_install_wayway -eq 2 ]];then
		install_mysql56_cmake
else
	echo "Input error for choose install Mysql way!"
	exit
fi
mysql_install_boot
}

#Define install mysql5.7
install_mysql57(){
if [[ -z $mysql_install_wayway || $mysql_install_wayway -eq 1 ]];then
	if [ $os == "centos" ];then
		install_mysql57_bin
	elif [ $os == "ubuntu" ];then
		install_mysql57_bin_u
	fi
elif [[ $mysql_install_wayway -eq 2 ]];then
		install_mysql57_cmake
else
	echo "Input error for choose install Mysql way!"
	exit
fi
mysql_install_boot
}

if [[ $mysql_version_select -eq 1 ]];then
	echo "You select install ${mysql_version[1]}"
	install_mysql56
elif [[ $mysql_version_select -eq 2 ]];then
	echo "You select install ${mysql_version[2]}"
	install_mysql57
fi
	
