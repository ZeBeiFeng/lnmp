#!/bin/bash
#####################################################################
# LNMP is a bash script for the installation of Nginx + PHP + MySQL.#
# Author        Hehl <1181554113@qq.com>                            #
# Blog          Http://www.yunweijilu.com                           #
#####################################################################
#php redis extensions lasts version is 3.1.4 20171115

#check php install and check redis.so installed
[ `whereis php | wc -l` -eq 0 ] && echo -e "${RED}Please install php first or link php to system path!${WHITE}" && exit 1

#Get php version and install
php_version=`php -v| head -n 1| awk '{print $2}' > /tmp/php_version.txt;cat /tmp/php_version.txt`

#Get Php install path
if [ `ps aux | grep php-fpm|grep -v grep|wc -l` -eq 0 ];then
        echo "Now start PHP-FPM process..."
        service php-fpm start
        [ $? -ne 0 ] && echo -e "${RED}Please install PHP first!!" && exit 1
else
        if [ `ls -l /usr/local/php | grep etc|wc -l` -eq 1 ];then
                php_path=`ps aux | grep php|grep -v grep | head -n1| awk '{print $14}'|tr -d "()"|awk -F"/" '{print $1,$2,$3,$4}'|sed 's/ /\//g'`
        else
                php_path=`ps aux | grep php|grep -v grep | head -n1| awk '{print $14}'|tr -d "()"|awk -F"/" '{print $1,$2,$3,$4,$5}'|sed 's/ /\//g'`
        fi
fi
echo -e "${GREEN}Now you use PHP version is: ${php_version} \nPhp Path is ${php_path}${WHITE} \n"

#check if add redis.so
[ `sed -n '/^extension*.*redis.so$/p' ${php_path}/etc/php.ini |wc -l` -eq 1 ] && echo -e "${RED} You had install redis.so as php extension!" && exit 0

#Install phpredis
pushd $src_dir
#[ ! -f ${phpredis_v}.tgz ] && wget http://pecl.php.net/get/redis-3.1.4.tgz
[ ! -d $phpredis_v ] && tar xzf ${phpredis_v}.tgz
cd $phpredis_v
${php_path}/bin/phpize
./configure --with-php-config=${php_path}/bin/php-config 
make && make install
if [ `ls -l ${php_path}/lib/php/extensions/ >/dev/null 2>&1| wc -l` -eq 0 ];then
	if [ `sed -n '/^extension*.*redis.so$/p' ${php_path}/etc/php.ini |wc -l` -ne 1 ];then
        	if [ `sed -n '/^extension_dir/p' ${php_path}/etc/php.ini |wc -l`  -eq 1 ];then
                	echo "extension = redis.so" >> ${php_path}/etc/php.ini
        	else
			extension_dir=`ls ${php_path}/lib/php/extensions/`
                	echo "extension = ${php_path}/lib/php/extensions/${extension_dir}/redis.so" >> ${php_path}/etc/php.ini
        	fi
	else
        	echo -e "${RED}Maybe you had installed redis.so in php.ini!${WHITE}"
	fi
else
	if [ `sed -n '/^extension*.*redis.so$/p' ${php_path}/etc/php.ini |wc -l` -eq 1 ];then
		echo "extension = redis.so" >> ${php_path}/etc/php.ini
	else
		extension_dir=`ls ${php_path}/lib/php/extensions/`
		echo "extension = ${php_path}/lib/php/extensions/${extension_dir}/redis.so" >> ${php_path}/etc/php.ini
	fi
fi
popd
