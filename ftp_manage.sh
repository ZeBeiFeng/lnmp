#!/bin/bash
clear
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
. include/common.sh
echo -e "$GREEN
#####################################################################
# LNMP is a bash script for the installation of Nginx + PHP + MySQL.#
# Author        Hehl <1181554113@qq.com>                            #
# Blog          Http://www.yunweijilu.com                           #
#####################################################################"

#Define user & password & directory
chk_ftp_user(){
while true;do
	echo -e "$GREEN"
	read -p "Please input ftp user name:" ftp_user
	if [ -z $ftp_user ];then
		echo -e "${RED}You input NULL user name${WHITE}"
	elif [[ `grep $ngx_user /etc/passwd | wc -l` -lt 1 ]];then
		useradd $ngx_user -s /sbin/nologin
	else
		break
	fi
done
}

chk_ftp_passwd(){
while true;do
	echo -e "$GREEN"
	read -p "Please input ftp password:" ftp_pwd
	if [ -z $ftp_pwd ];then
		echo -e "${RED}You input NULL password${WHITE}"
	elif [[ `echo $ftp_pwd | wc -c` -lt 5 ]];then
			echo "You must input 5 character!"
	else
		break
	fi
done
}

chk_ftp_home(){
while true;do
	echo -e "$GREEN"
	read -p "Please input ftp user directory:(example:$wwwroot_dir)    " ftp_user_home
	if [ -z $ftp_user_home ];then
		echo -e "${RED}You input NULL ftp directory${WHITE}"
	elif [ ! -d $ftp_user_home ];then
		echo -e "There is no such directory. I will create it."
		mkdir -p $ftp_user_home
		break
	else
		break
	fi
done
}

while :;do
	echo
	echo
	echo -e "${YELLOW}Please input number(1-6 or q):
\t${YELLOW}1${WHITE}. Create new user
\t${YELLOW}2${WHITE}. Change user dirctory
\t${YELLOW}3${WHITE}. Reset user password
\t${YELLOW}4${WHITE}. Delete user
\t${YELLOW}5${WHITE}. List all user
\t${YELLOW}6${WHITE}. Show user
\t${YELLOW}q${WHITE}. Exit"
	read -p "Please input the number: " Number
	if [[ $Number =~  ^[1-6,q]$ ]];then
		case $Number in
		1)
			chk_ftp_user
			chk_ftp_passwd
			chk_ftp_home
			echo -e "${ftp_pwd}\n${ftp_pwd}" | $ftp_pw useradd $ftp_user  -u $ngx_user -g $ngx_group -d $ftp_user_home -m >/dev/null 2>&1
			$ftp_pw mkdb $ftp_pdb -f $ftp_passfile 
			echo -e "$ftp_user create successful!"
			echo -e "$ftp_user directory is ${WARN}$ftp_user_home \n$ftp_user${WHITE} password is ${WARN}${ftp_pwd}${WHITE}"
		;;
		2)
			chk_ftp_user
			chk_ftp_home	
			$ftp_pw usermod $ftp_user -f $ftp_passfile -d $ftp_user_home -m
			$ftp_pw mkdb $ftp_pdb -f $ftp_passfile 
		;;
		3)
			chk_ftp_user
			chk_ftp_passwd
			echo -e "${ftp_pwd}\n${ftp_pwd}" | $ftp_pw passwd $ftp_user -f $ftp_passfile -m >/dev/null 2>&1
			$ftp_pw mkdb $ftp_pdb -f $ftp_passfile 
		;;
		4)
			chk_ftp_user
			$ftp_pw userdel $ftp_user -f $ftp_passfile -m
		;;
		5)
			echo -e "User name and directory is:$GREEN"
			$ftp_pw list -f ${ftp_passfile}
		;;
		6)
			chk_ftp_user
			$ftp_pw show $ftp_user
		;;
		q)
			exit
		;;
		esac
	fi

done
