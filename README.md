# 欢迎使用 LNMP一键部署脚本
---
**一.简介**
LNMP一键部署脚本是一键部署web环境所用，非常适合网站运维人员，web开发人员在Linux系统中部署web环境.我的博客 http://www.yunweijilu.com

您可以使用LNMP一键部署脚本：
> * 自动部署Nginx,Mysql,Php
> * 备份数据库
> * 升级软件版本
> * 添加虚拟站点和目录  

**支持系统:**  
CentOS 6.x  
CentOS 7.x  
Ubuntu 14.x  
Ubuntu 16.x  

**正式版下载**  
https://gitee.com/hehl/lnmp/repository/archive/v1.0.1.zip  

**最新版**  
```
git clone https://gitee.com/hehl/lnmp.git  
```  
root用户一条命令安装lnmp(暂仅安装nginx1.12, mysql5.6, Php7.1,如需增加其他自动安装，请参考include/Menu.sh中No_Menu )  
```  
curl -sL http://www.yunweijilu.com/software/lnmp.sh | bash
```  
一条命令安装的mysql密码默认为yunweijilu.com  

**二.文件目录**

```
conf                                   #软件配置文件夹
include                                #系统软件部署脚本文件夹
init.d                                 #服务脚本文件夹
src                                    #软件包文件夹
db_backup_import.sh                    #数据导出和导入脚本
install.sh                             #LNMP一键部署脚本
uninstall.sh                           #一键卸载脚本
upgrade.sh                             #一键升级脚本
vhost.sh                               #一键增加Nginx虚拟站点和网站目录
php_swich.sh                           #一键切换Php版本
reset_mysql_pwd.sh                     #一键重置Mysql账号root的密码

```
**三.安装截图**

### 文件

![安装菜单][1]

### 安装

![菜单一][2]

![菜单二][3]

### 安装完成

![完成][4]

### 站点添加

![添加站点][5]

### 数据库提示

![数据库提示][6]

### 数据库备份

![数据库备份][7]

### LNMP卸载

![uninstall][8]

[1]: http://imgs.yunweijilu.com/img/file.png
[2]: http://imgs.yunweijilu.com/img/menu1.png
[3]: http://imgs.yunweijilu.com/img/menu2.png
[4]: http://imgs.yunweijilu.com/img/finished.png
[5]: http://imgs.yunweijilu.com/img/vhost.png
[6]: http://imgs.yunweijilu.com/img/db1.png
[7]: http://imgs.yunweijilu.com/img/db2.png
[8]: http://imgs.yunweijilu.com/img/uninstall.png


### 更新记录  

201711
增加一条命令直接安装lnmp，无需交互式询问

201709
增加MySQL数据库密码一键重置脚本;
增加php版本一键切换,支持最新版本7.1.9,自动安装指定版本,如已经安装,再次来回切换无需重新安装

201708
增加ftp软件pureftp的安装部署

201707
增加支持ubuntu16
增加redis部署和管理

201706
增加支持ubuntu14

201705
开始写这个脚本

### 注:
****
1.建议安装git克隆项目,方便以后拉取最新版本  
2.Mysql百度盘下载链接：http://pan.baidu.com/s/1qY2ZJB2 密码：ea5x  
3.有任何问题和建议[点我](https://gitee.com/hehl/lnmp/issues)提交  
4.脚本运行中如果无法退出，请按Ctrl+c组合键退出
作者博客 http://www.yunweijilu.com  

